package com.josh.usermanagementsystembackend.controller;

import com.josh.usermanagementsystembackend.exception.UserNotFoundException;
import com.josh.usermanagementsystembackend.model.User;
import com.josh.usermanagementsystembackend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin("http://localhost:5173")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @PostMapping("/add-user")
    public User addUser(@RequestBody User user){
        return userRepository.save(user);
    }

    @GetMapping("/latestID")
    public ResponseEntity<String> getLatestUserID(){
        String latestUserID = userRepository.findMaxUserID();

        return ResponseEntity.ok(latestUserID);
    }

    @GetMapping("/users")
    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    @GetMapping("/users/{userID}")
    public User getUser(@PathVariable String userID){
        return userRepository.findById(userID)
                .orElseThrow(()->new UserNotFoundException(userID));
    }

    @PutMapping("users/edit/{userID}")
    public User updateUser(@RequestBody User updatedUser, @PathVariable String userID){
        return userRepository.findById(userID)
                .map(user->{
                    user.setUserID(updatedUser.getUserID());
                    user.setFirstName(updatedUser.getFirstName());
                    user.setLastName(updatedUser.getLastName());
                    user.setBirthday(updatedUser.getBirthday());
                    user.setEmail(updatedUser.getEmail());

                    user.setIsLasallian(updatedUser.getIsLasallian());
                    user.setIsCSStudent(updatedUser.getIsCSStudent());
                    user.setFavoriteColor(updatedUser.getFavoriteColor());
                    user.setFavoriteMonth(updatedUser.getFavoriteMonth());
                    user.setAge(updatedUser.getAge());
                    user.setMoodToday(updatedUser.getMoodToday());
                    user.setTimeToWakeUp(updatedUser.getTimeToWakeUp());
                    user.setSalary(updatedUser.getSalary());
                    user.setDel(updatedUser.getDel());

                    user.setTextDocFileName(updatedUser.getTextDocFileName());
                    user.setTextDocContentType(updatedUser.getTextDocContentType());
                    user.setTextDocFileSize(updatedUser.getTextDocFileSize());

                    user.setAvatarFileName(updatedUser.getAvatarFileName());
                    user.setAvatarContentType(updatedUser.getAvatarContentType());
                    user.setAvatarFileSize(updatedUser.getAvatarFileSize());

                    user.setPassword(updatedUser.getPassword());

                    return userRepository.save(user);

                }).orElseThrow(()->new UserNotFoundException(userID));
    }

}

package com.josh.usermanagementsystembackend.model;


import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
public class User {
    @Id
    private String userID;
    private String firstName;
    private String lastName;
    private LocalDate birthday;
    private String email;

    private int isLasallian;
    private int isCSStudent;
    private String favoriteColor;
    private String favoriteMonth;
    private int age;
    private int moodToday;
    private LocalTime timeToWakeUp;
    private float salary;
    private int del;

    private String textDocFileName;
    private String textDocContentType;
    private float textDocFileSize;

    private String avatarFileName;
    private String avatarContentType;
    private float avatarFileSize;

    private String password;

    public User() {
    }

    public User(String userID, String firstName, String lastName, LocalDate birthday, String email, int isLasallian,
                int isCSStudent, String favoriteColor, String favoriteMonth, int age, int moodToday, LocalTime timeToWakeUp,
                float salary, int del, String textDocFileName, String textDocContentType, float textDocFileSize,
                String avatarFileName, String avatarContentType, float avatarFileSize, String password) {
        this.userID = userID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.email = email;
        this.isLasallian = isLasallian;
        this.isCSStudent = isCSStudent;
        this.favoriteColor = favoriteColor;
        this.favoriteMonth = favoriteMonth;
        this.age = age;
        this.moodToday = moodToday;
        this.timeToWakeUp = timeToWakeUp;
        this.salary = salary;
        this.del = del;
        this.textDocFileName = textDocFileName;
        this.textDocContentType = textDocContentType;
        this.textDocFileSize = textDocFileSize;
        this.avatarFileName = avatarFileName;
        this.avatarContentType = avatarContentType;
        this.avatarFileSize = avatarFileSize;
        this.password = password;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIsLasallian() {
        return isLasallian;
    }

    public void setIsLasallian(int isLasallian) {
        this.isLasallian = isLasallian;
    }

    public int getIsCSStudent() {
        return isCSStudent;
    }

    public void setIsCSStudent(int isCSStudent) {
        this.isCSStudent = isCSStudent;
    }

    public String getFavoriteColor() {
        return favoriteColor;
    }

    public void setFavoriteColor(String favoriteColor) {
        this.favoriteColor = favoriteColor;
    }

    public String getFavoriteMonth() {
        return favoriteMonth;
    }

    public void setFavoriteMonth(String favoriteMonth) {
        this.favoriteMonth = favoriteMonth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getMoodToday() {
        return moodToday;
    }

    public void setMoodToday(int moodToday) {
        this.moodToday = moodToday;
    }

    public LocalTime getTimeToWakeUp() {
        return timeToWakeUp;
    }

    public void setTimeToWakeUp(LocalTime timeToWakeUp) {
        this.timeToWakeUp = timeToWakeUp;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public int getDel() {
        return del;
    }

    public void setDel(int del) {
        this.del = del;
    }

    public String getTextDocFileName() {
        return textDocFileName;
    }

    public void setTextDocFileName(String textDocFileName) {
        this.textDocFileName = textDocFileName;
    }

    public String getTextDocContentType() {
        return textDocContentType;
    }

    public void setTextDocContentType(String textDocContentType) {
        this.textDocContentType = textDocContentType;
    }

    public float getTextDocFileSize() {
        return textDocFileSize;
    }

    public void setTextDocFileSize(float textDocFileSize) {
        this.textDocFileSize = textDocFileSize;
    }

    public String getAvatarFileName() {
        return avatarFileName;
    }

    public void setAvatarFileName(String avatarFileName) {
        this.avatarFileName = avatarFileName;
    }

    public String getAvatarContentType() {
        return avatarContentType;
    }

    public void setAvatarContentType(String avatarContentType) {
        this.avatarContentType = avatarContentType;
    }

    public float getAvatarFileSize() {
        return avatarFileSize;
    }

    public void setAvatarFileSize(float avatarFileSize) {
        this.avatarFileSize = avatarFileSize;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

package com.josh.usermanagementsystembackend.repository;

import com.josh.usermanagementsystembackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository <User, String>{

    @Query("SELECT MAX(u.userID) FROM User u")
    String findMaxUserID();

}

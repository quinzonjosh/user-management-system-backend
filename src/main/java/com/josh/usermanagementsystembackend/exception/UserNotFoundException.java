package com.josh.usermanagementsystembackend.exception;

public class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(String userID) {
        super("user "+userID+" not found");
    }
}
